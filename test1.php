<?php
	require('dbcon.php');

?>

<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<script>
		function find(str) {
			  if (str.length == 0) {
				document.getElementById("print").innerHTML = "";
				return;
			  } else {
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
				  if (this.readyState == 4 && this.status == 200) {
					document.getElementById("print").innerHTML = this.responseText;
				  }
				};
				xmlhttp.open("GET", "page2.php?q=" + str, true);
				xmlhttp.send();
			  }
			}
		</script>
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>

		<!-- Wrapper -->
			<div id="wrapper">
				<form action="studviewjobs.php" method="GET">
					<table>
						<tr>
							<td>Category: </td><td><select name="industry" onchange="find(this.value)">
													<option>All categories</option>
													<option>Aerospace Industry</option>
													<option>Transport Industry</option>
													<option>Engineering</option>
													<option>Computers</option>
													<option>Telecommunication</option>
													<option>Agriculture</option>
													<option>Construction</option>
													<option>Education</option>
													<option>Pharmaceutical</option>
													<option>Food</option>
													<option>Health Care</option>
													<option>Hospitality</option>
													<option>Entertainment</option>
													<option>News Media</option>
													<option>Energy</option>
													<option>Manufacturing</option>
													<option>Music</option>
													<option>Electronics</option>
											</select></td>
											<td><input type="submit" value="Search"></button></td>
						</tr>
					</table>
				</form action="">
				<!-- Main -->
					<section id="main" class="wrapper">
						<div class="inner">
							<table>
								<th>Job Title</th><th>No. of Applicants</th><th>Deadline</th>
								<span id="print"></span>
							</table>
						</div>
					</section>

			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>




