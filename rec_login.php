<!DOCTYPE HTML>
<?php
	require('dbcon.php');
?>
<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<section id="sidebar">
						<div class="inner">
							<h3 align="right">Welcome </h3>
							<nav>
								<ul>
									<li><a align="right" href="index.html" >Home</a></li>
									<li><a align="right" href="rec_reg.php" >Create Account</a></li>
									<li><a align="right" href="recresetpass.php" >Forgot Password</a></li>
								</ul>
							</nav>
						</div>
					</section>
		<!-- Header -->
			
		<!-- Wrapper -->
			<div id="wrapper" style="background-image: url('images/740570.jpg'); background-size: cover; background-repeat: no-repeat">
				<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>

				<!-- Main -->
					<section id="main" class="wrapper">
						<div class="inner">
							<h1 align="center">Recruiter Login</h1>
							<form method="post" action="rec_logincheck.php">
								<table align="center">
										<tr>
											<th align="left">Email or id:         </th>
											<td><input type="email" name="email"></td>
										</tr>
										<tr>
											<th align="left">Password:     </th>
											<td><input type="password" name="password"></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
											<br>
											<input type="submit" value="     Log in    ">
											</td>
										</tr>
								</table>
							</form>
						</div>
					</section>
					<br><br><br><br>
			</div>

		

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>