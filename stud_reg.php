<!DOCTYPE HTML>
<?php
	require('dbcon.php');
	$sql = "SELECT * FROM degrees";
														$result = mysqli_query($con,$sql);
														$count = mysqli_num_rows($result);
														$i=1;
?>
<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<!-- Header -->
			
		<!-- Wrapper -->
		<section id="sidebar">
						<div class="inner">
							<h3 align="right">Welcome </h3>
							<nav>
								<ul>
									<li><a align="right" href="index.html" >Home</a></li>
									<li><a align="right" href="stud_login.php" >Sign In</a></li>
								</ul>
							</nav>
						</div>
					</section>
			<div id="wrapper">
				<!-- Main -->
					<header id="header">
						<a href="index.html" class="title">Job Placement System</a>
					</header>
					<section id="main" class="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
						<h1 align="center">Student Registration</h1>
 
						<form method="POST" action="stud_registration.php">  
								<table> 	
									<tr><td>Name:</td><td><input type="text" name="name" required id="name" required></td></tr>
									<tr><td>Date of birth:</td><td><input STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="date" name="dob" required id="dob" required></td></tr>
									<tr><td>Email:</td><td><input type="email" name="email" required id="email"></td></tr>
									<tr><td>Phone:</td><td><input STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="number" name="phone" required ></td></tr>
									<tr><td>Level:</td><td><select name="level">
																				<option>Diploma</option>
																				<option>Bachelors Degree</option>
																				<option>Masters Degree</option>
																				<option>Doctoral Degree</option>
																		</select></td></tr>
									<tr><td>Degree:</td><td><select name="degree" class="form-control">
																				<?php 
																					//display list of hostels in the database
																					$sql = "SELECT * FROM degrees";
																					$result = mysqli_query($con,$sql);
																					$count = mysqli_num_rows($result);
																					echo $count,"<br>";
																					if($count<=0)
																					{
																						echo "<option>No degrees available</option>";
																					}
																					else
																					{
																					while ($row = mysqli_fetch_array($result))
																					{
																						echo "<option value='" . $row['id'] ."'>".$row['name']."</option>";
																					}
																					}
																				?>
																		</select></td></tr>
									<tr><td>Graduation year:</td><td><input class="form-control" STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="number" name="grad_year" required pattern=".{3,}" maxlength="4" min="2000" max="3000"></td></tr>
									<tr><td>Password:</td><td><input type="password" name="password" id="pass" onkeyup="validate()" required ></td></tr>
									<tr><td>Confirm Password:</td><td><input type="password" name="cpassword" id="cpass" required></td></tr>
									<tr><td><span id = "message"></span></td></tr>
									<tr><td colspan="2" align="center"><input type="submit" name="submit"></td></tr>
								</table>
							</form>
    
						</div>
					</section>

			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>