<!DOCTYPE HTML>
<?php
	require('dbcon.php');
?>
<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="widgEditor/css/widgEditor.css" />
				<script src="ckeditor/ckeditor.js"></script>
		<script src="widgEditor/scripts/widgEditor.js"></script>
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
		<!-- Wrapper -->
			<div id="wrapper" >
				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
						<h1 align="center">Create Resume</h1>
 
						<form method="POST" action="createresumeback.php">  
								<table> 	
									<tr><td>Name:</td><td><input type="text" name="name" required id="name" required></td></tr>
									<tr><td>Date of birth:</td><td><input STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="date" name="dob" required id="dob" required></td></tr>
									<tr><td>Email:</td><td><input type="email" name="email" required id="email"></td></tr>
									<tr><td>Phone:</td><td><input STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="number" name="phone" required ></td></tr>
									
									<tr><td>Degree:</td><td><select name="degree" class="form-control">
																				<option>Bachelor of Architecture - B.Arch
																					</option>
																							<option>Bachelor of Arts - B.A.    
																							</option>
																							<option>Bachelor of Ayurvedic Medicine & Surgery - B.A.M.S.    
																							</option>
																							<option>Bachelor of Business Administration - B.B.A.
																							</option>
																							<option>Bachelor of Commerce - B.Com.
																							</option>
																							<option>Bachelor of Computer Applications - B.C.A.
																							</option>
																							<option>Bachelor of Dental Surgery - B.D.S.
																							</option>
																							<option>Bachelor of Design - B.Des. / B.D.
																							</option>
																							<option>Bachelor of Education - B.Ed.
																							</option>
																							<option>Bachelor of Engineering / </option><option>Bachelor of Technology - B.E./B.Tech.
																							</option>
																							<option>Bachelor of Fine Arts - BFA / BVA
																							</option>
																							<option>Bachelor of Fisheries Science - B.F.Sc./ B.Sc. (Fisheries).
																							</option>
																							<option>Bachelor of Homoeopathic Medicine and Surgery - B.H.M.S.
																							</option>
																							<option>Bachelor of Laws - L.L.B.
																							</option>
																							<option>Bachelor of Library Science - B.Lib. / B.Lib.Sc.
																							</option>
																							<option>Bachelor of Mass Communications - B.M.C. / B.M.M.
																							</option>
																							<option>Bachelor of Medicine 
																							</option><option>Bachelor of Surgery - M.B.B.S.
																							</option>
																							<option>Bachelor of Nursing
																							</option>
																							<option>Bachelor of Pharmacy - B.Pharm / B.Pharma.
																							</option>
																							<option>Bachelor of Physical Education - B.P.Ed.
																							</option>
																							<option>Bachelor of Physiotherapy - B.P.T.
																							</option>
																							<option>Bachelor of Science - B.Sc.
																							</option>
																							<option>Bachelor of Social Work - BSW / B.A. (SW)
																							</option>
																							<option>Bachelor of Veterinary Science & Animal Husbandry - B.V.Sc. & A.H. / B.V.Sc
																							</option>
																							<option>Doctor of Medicine - M.D.
																							</option><option>Doctor of Medicine in Homoeopathy - M.D. (Homoeopathy)
																							</option><option>Doctor of Pharmacy - Pharm.D    
																							</option><option>Doctor of Philosophy - Ph.D.
																							</option><option>Doctorate of Medicine - D.M.
																							</option>
																							<option>Master of Architecture - M.Arch.
																							</option><option>Master of Arts - M.A.
																							</option><option>Master of Business Administration - M.B.A.
																							</option><option>Master of Chirurgiae - M.Ch.
																							</option><option>Master of Commerce - M.Com.    
																							</option><option>Master of Computer Applications - M.C.A.
																							</option><option>Master of Dental Surgery - M.D.S.
																							</option><option>Master of Design - M.Des./ M.Design.
																							</option><option>Master of Education - M.Ed.
																							</option><option>Master of Engineering / </option><option>Master of Technology - M.E./ M.Tech.
																							</option><option>Master of Fine Arts - MFA / MVA
																							</option><option>Master of Laws - L.L.M.
																							</option><option>Master of Library Science - M.Lib./ M.Lib.Sc.
																							</option><option>Master of Mass Communications / Mass Media - M.M.C / M.M.M.
																							</option><option>Master of Pharmacy - M.Pharm    
																							</option><option>Master of Philosophy - M.Phil.    
																							</option><option>Master of Physical Education M.P.Ed. / M.P.E.
																							</option><option>Master of Physiotherapy - M.P.T.
																							</option><option>Master of Science - M.Sc.
																							</option><option>Master of Social Work / </option><option>Master of Arts in Social Work - M.S.W. / M.A. (SW)
																							</option><option>Master of Science in Agriculture - M.Sc. (Agriculture)
																							</option><option>Master of Surgery - M.S.
																							</option><option>Master of Veterinary Science - M.V.Sc.</option>
																		</select></td></tr>
									<tr style="color: white"><td>From:</td><td><input class="form-control" STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="date" name="from_year" required>       &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp    To:<input class="form-control" STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="date" name="to_year" required></td></tr>
									<tr><td>Write something about yourself:</td><td><textarea name="about" id="editor1" rows="10" cols="80" >
													Please make sure to include all the details of the job responsibilities, requirents and more. Make use of bullets to make your description more presentable.
														Your job posting may not be approved if it is ambigous or not presentable. 
												</textarea>
												<script>
													// Replace the <textarea id="editor1"> with a CKEditor 4
													// instance, using default configuration.
													CKEDITOR.replace( 'editor1' );
												</script></td></tr>
									<tr><td>Facebook profile link:</td><td><input type="text" name="fb_profile"></td></tr>
									<tr><td>Skill 1:</td><td><input type="text" name="skill1"></td><tr>
									<tr><td>Skill 2:</td><td><input type="text" name="skill2"></td><tr>
									<tr><td>Skill 3:</td><td><input type="text" name="skill3"></td><tr>
									<tr><td colspan="2" align="center"><input type="submit" name="submit"></td></tr>
								</table>
							</form>
    
						</div>
					</section>

			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>