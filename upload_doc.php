<!DOCTYPE HTML>
<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])) {
		$id = $_SESSION['id'];
	} else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: stud_login.php');
	}
?>

<html>
	<head>
		<title>Upload Documents</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome </h3><h2><?php echo $_SESSION['name'];?> </h2>
					<nav>
						<ul>
							<li><a align="right" href="studviewjobs.php" >Job Posts</a></li>
							<li><a align="right" href="myapps.php" >My Applications</a></li>
							<li><a align="right" href="viewdocs.php" >Documents</a></li>
							<li><a align="right" href="stud_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		<!-- Header -->
			
			
		<!-- Wrapper -->
			<div id="wrapper">
					<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
				
			</header>
				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
						<h1 align="center">Upload Documents</h1>
						<form method="post" action="uploaddoc.php" enctype="multipart/form-data">
<table align="center">
<tr>
	<th align="left" style="font-size:25px;">Document         </th>
	<td><select name="document">
													<option>Graduation Certificate</option>
													<option>Student Marksheet</option>
													<option>Identification Documents</option>
													<option>Other</option>
											</select></td>
</tr>
<tr>
	<th align="left" style="font-size:25px;">File     </th>
	<td><input type="file" name="file" required ></td>
</tr>
<tr>
	<td colspan="2" align="center">
	<input type="submit" value="Upload">
	</td>
</tr>

<tr>
	<td><br><p><a   href="stud_home.php">< Back to Home</a></p></td>
	<td><br><p align="right"><a   href="viewdocs.php">View Documents ></a></p></td>
</tr>
</table>
</form>
						
						
						</div>
					</section>

			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Untitled. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>