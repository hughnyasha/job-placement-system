<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])) {
		$id = $_SESSION['id'];
	} else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: rec_login.php');
	}
														$job_id=$_GET['title'];
														$sql = "SELECT * FROM jobs where id=$job_id and rec_id=$id";
														$result = mysqli_query($con,$sql);
														$count = mysqli_num_rows($result);
														if(!$result)
														{
															die();
														}
														else{
															if($count<=0)
															{
																echo "<option>No degrees available</option>";
															}
															$row = mysqli_fetch_array($result);
														}
	
?>
	

<html>
	<head>
		<title>Home</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
			<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome </h3><h2><?php echo $_SESSION['name'];?> </h2>
					<nav>
						<ul>
							<li><a align="right" href="rec_home.php" >Home</a></li>
							<li><a align="right" href="recviewjob.php" >Job Posts</a></li>
							<li><a align="right" href="postjob.php" >Post New Job</a></li>
							<li><a align="right" href="rec_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		<!-- Header -->
			
			<div id="wrapper">
				<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
				
			</header>
				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/740570.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
								<h2 align="center">Job View</h2>
								<table align="center">
									<?php 
														
														//display list of hostels in the database
														$sql = "SELECT * FROM jobs where id=$job_id and rec_id=$id";
														$result = mysqli_query($con,$sql);
														$count = mysqli_num_rows($result);
														if($count<=0)
														{
															echo "<option>No degrees available</option>";
														}
														else
														{
														$row = mysqli_fetch_array($result);
															$id=$row['id'];
															echo "<tr><td>Title:</td><td>".$row['title']."</td></tr>";
															echo "<tr><td>Company:</td><td>".$row['company']."</td></tr>";
															echo "<tr><td>Location:</td><td>".$row['Place']."</td></tr>";
															echo "<tr><td>Job Type:</td><td>".$row['job_type']."</td></tr>";
															echo "<tr><td>Work Type:</td><td>".$row['work_type']."</td></tr>";
															echo "<tr><td>Salary:</td><td>".$row['salary']."</td></tr>";
															echo "<tr><td>Description:</td><td>".$row['description']."</td></tr>";
															echo "<tr><td>Deadline:</td><td>".$row['deadline']."</td></tr>";
														}
													?>
										<td colspan="2" align="center">
											<button><?php echo "<a href='recupdatepost.php?title=$id'>";?>Update Post</a></button> &nbsp <button><?php echo "<a href='recviewapplicants.php?id=$job_id'>";?>View Applicants</a></button> &nbsp <button><?php echo "<a href='recdelpost.php?id=$id'>";?>Delete Post</a></button>
										</td>
									</tr>
									
								</table>
							
						</div>
					</section>
			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>


