<!DOCTYPE HTML>
<html>
	<head>
		<title>Reset Password - JPS</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Header -->
			
		
		<!-- Wrapper -->
			
					<section id="sidebar">
						<div class="inner">
							<h3 align="right">Welcome </h3>
							<nav>
								<ul>
									<li><a align="right" href="index.html" >Home</a></li>
									<li><a align="right" href="stud_login.php" >Sign In</a></li>
									<li><a align="right" href="stud_reg.php" >Create Account</a></li>
									
								</ul>
							</nav>
						</div>
					</section>
			<div id="wrapper">
					<header id="header">
						<a href="index.html" class="title">Job Placement System</a>
					</header>
				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
							<br>
							<h1 align="center">Reset Password</h1>
							<form method="post" action="studpassreset.php">
								<table align="center">
									<tr>
										<th align="left">Email         </th>
										<td><input type="email" name="email" required></td>
									</tr>
									<tr>
										<th align="left">Phone     </th>
										<td><input style="background-color:indigo" type="number" name="phone" required></td>
									</tr>
									<tr>
										<th align="left">D.O.B     </th>
										<td><input style="background-color:indigo" type="date" name="dob" required></td>
									</tr>
									<tr>
										<td colspan="2" align="center">
										<input type="submit" value="Reset Password">
										</td>
									</tr>
								</table>
							</form>
						</div>
						<br><br><br><br><br><br>
					</section>

			</div>


		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>