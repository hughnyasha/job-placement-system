<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])) {
		$id = $_SESSION['id'];
	} else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: stud_login.php');
	}
?>
	

<html>
	<head>
		<title>Documents - JPS</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
			<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome </h3><h2><?php echo $_SESSION['name'];?> </h2>
					<nav>
						<ul>
							<li><a align="right" href="studviewjobs.php" >Job Posts</a></li>
							<li><a align="right" href="myapps.php" >My Applications</a></li>
							<li><a align="right" href="upload_doc.php" >Upload Documents</a></li>
							<li><a align="right" href="stud_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		<!-- Header -->
			
			<div id="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
				<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
				
			</header>
				
				<!-- Main -->
					<section id="main" class="wrapper">		
						<div class="inner">
							<h1 align="center">Documents</h1>
							<form method="post" action="viewdocsback.php">
								<table align="center">
									<tr><td>Document</td><td>
										<select name="document" required><?php
										$sql = "SELECT * FROM documents where stud_id='$id'";
																							$result = mysqli_query($con,$sql);
																							$count = mysqli_num_rows($result);
																							
																							if($count<=0)
																							{
																								echo "<option>No documents available</option>";
																							}
																							else
																							{
																							while ($row = mysqli_fetch_array($result))
																							{
																								echo "<option value='" . $row['name'] ."'>".$row['name']."</option>";
																							}
																							}
																							?>
										</select></td></tr>
									<tr><td>Action</td><td><select name="action" required><option>View</option>
																								<option>Delete</option>
										</select></td>
									</tr>
									<tr>
										<td colspan="2" align="center">
											<input type="submit" value="Proceed">
										</td>
									</tr>
									<tr>
										<td colspan="2"><br><p><a   href="stud_home.php">< Back to Home</a></p></td>
									</tr>
								</table>
							</form>
						</div>
					</section>
			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>