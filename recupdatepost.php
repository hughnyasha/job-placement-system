<!DOCTYPE HTML>
<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])) {
		$id = $_SESSION['id'];
		$job_id= $_GET['title'];
		
		$sql = "SELECT * FROM jobs where id=$job_id and rec_id=$id";
														$result = mysqli_query($con,$sql);
														$count = mysqli_num_rows($result);
														if($count<=0)
														{
															echo "No Post found!";
														}
														else
														{
														$row = mysqli_fetch_array($result);
														$title = $row['title'];
														$job_type = $row['job_type'];
														$salary = $row['salary'];
														$time = $row['work_type'];														
														$place = $row['Place'];
														$description = $row['description'];
														$deadline = $row['deadline'];
											
														}
	} 
	else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: rec_login.php');
	}
?>

<html>
	<head>
		<title>Job Post</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="widgEditor/css/widgEditor.css" />
				<script src="ckeditor/ckeditor.js"></script>
		<script src="widgEditor/scripts/widgEditor.js"></script>
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
			<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome <?php echo $_SESSION['name'];?></h3>
					<nav>
						<ul>
							<li><a align="right" href="rec_home.php" >Home</a></li>
							<li><a align="right" href="recviewjob.php" >Job Posts</a></li>
							<li><a align="right" href="postjob.php" >Post new job</a></li>
							<li><a align="right" href="recacc.php" >Account</a></li>
							<li><a align="right" href="rec_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		<!-- Wrapper -->
			<div id="wrapper" style="background-image: url('images/740570.jpg'); background-size: cover; background-repeat: no-repeat">

				<!-- Main -->
					<section id="main" class="wrapper">
						<div class="inner">
							<h1 align="center">Job Post</h1>
							<?php echo "<form method='post' action='recupdatepostback.php?id=$job_id' enctype='multipart/form-data'>";?>
								<table align="center">
									<tr><td>Job Title:</td><td><?php echo "<input type='text' name='title' value='$title' required>";?></td></tr>
									<tr>
										<td>Job Type:</td><td><?php echo "<select name='job_type' value='$job_type'>
																						<option>Job</option>
																						<option>Internship</option>
																				</select></td>";?>
									</tr>
									<tr><td>Salary:</td><td><?php echo "<input type='number' name='salary' value='$salary' required style='background-color: indigo;'>";?></td></tr>
									<tr><td>Time:</td><td><select name="time">
																						<option>Full Time</option>
																						<option>Part Time</option>
																				</select></td></tr>
									<tr><td>Place:</td><td><?php echo "<input type='text' name='place' value='$place' required>";?></td></tr>
									<tr><td>Description:</td><td><?php echo "<textarea name='description' id='editor1' value='$description' rows='10' cols='80'>";?>
													Please make sure to include all the details of the job responsibilities, requirents and more. Make use of bullets to make your description more presentable.
														Your job posting may not be approved if it is ambigous or not presentable. 
												</textarea>
												<script>
													// Replace the <textarea id="editor1"> with a CKEditor 4
													// instance, using default configuration.
													CKEDITOR.replace( 'editor1' );
												</script></td></tr>
									<tr><td>Deadline:</td><td><?php echo "<input type='date' name='deadline' required style='background-color:indigo;' value='$deadline'>";?></td></tr>
									<tr>
										<td colspan="2" align="center">
										<input type="submit" value="Update">
										</td>
									</tr>

									
								</table>
							</form>
						</div>
					</section>
			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Untitled. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>