<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
	require('../dbcon.php');
	session_start();
	if (isset($_SESSION['name'])) {
		$id = $_SESSION['id'];
		$job_id = $_GET['job_id'];
		$stud_id = $_GET['id'];
		$sql = "Select * from resumes where id=stud_id";
		$result = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($result);
		
		$name = $row['name'];
		$email = $row['email'];
		$phone = $row['phone'];
		$dob = $row['dob'];
		$about = $row['about'];
		$degree = $row['degree'];
		$fb_profile = $row['fb_profile'];
		$from_year = $row['from_year'];
		$to_year = $row['to_year'];
		$skill1 = $row['skill1'];
		$skill2 = $row['skill2'];
		$skill3 = $row['skill3'];
$file = "pic.jpg";		
$filename = '../documents/'.$file;
		
	} else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: ../stud_login.php');
	}
?>




<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Resume</title>
		<link type="text/css" rel="stylesheet" href="css/purple.css" />
		<link type="text/css" rel="stylesheet" href="css/print.css" media="print"/>
		<!--[if IE 7]>
		<link href="css/ie7.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if IE 6]>
		<link href="css/ie6.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.tipsy.js"></script>
		<script type="text/javascript" src="js/cufon.yui.js"></script>
		<script type="text/javascript" src="js/scrollTo.js"></script>
		<script type="text/javascript" src="js/myriad.js"></script>
		<script type="text/javascript" src="js/jquery.colorbox.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<script type="text/javascript">
				Cufon.replace('h1,h2');
		</script>
	</head>
	<body>
		<!-- Begin Wrapper -->
		<div id="wrapper">
			<div class="wrapper-top"></div>
			<div class="wrapper-mid">
			<!-- Begin Paper -->
				<div id="paper">
					<div class="paper-top"></div>
					<div id="paper-mid">
						<div class="entry">
							<!-- Begin Image -->
							<a href="viewpic.php"><img class="portrait" src="../documents/pic.jpg"  alt="<?php echo $name; ?>" /></a>
							<!-- End Image -->
							<!-- Begin Personal Information -->
							<div class="self">
								<h1 class="name"><?php echo $name;?> <br />
								<span>Student</span></h1>
								<ul>
								  <li class="ad">505 INT BOYS Hostel, Parul University</li>
								  <li class="mail"><?php echo $email;?></li>
								  <li class="tel"><?php echo $phone;?></li>
								  
								</ul>
							</div>
							  <!-- End Personal Information -->
							  <!-- Begin Social -->
							<div class="social">
								<ul>
									<li><a class='north' href="#" title="Download .pdf"><img src="images/icn-save.jpg" alt="Download the pdf version" /></a></li>
									<li><a class='north' href="javascript:window.print()" title="Print"><img src="images/icn-print.jpg" alt="" /></a></li>
									<li><a class='north' id="contact" href="contact/index.html" title="Contact Me"><img src="images/icn-contact.jpg" alt="" /></a></li>
									<li><a class='north' href="#" title="Follow me on Twitter"><img src="images/icn-twitter.jpg" alt="" /></a></li>
									<li><a class='north' href="<?php echo $fb_profile;?>" title="My Facebook Profile"><img src="images/icn-facebook.jpg" alt="" /></a></li>
								</ul>
							</div>
						  <!-- End Social -->
						</div>
						<!-- Begin 1st Row -->
						<div class="entry">
							<h2>ABOUT</h2>
							<?php echo $about; ?>
						</div>
						
						<div class="entry">
							<h2>D.O.B</h2>
							<big><p><?php echo $dob; ?></p></big>
						</div>
						
						<!-- End 1st Row -->
						<!-- Begin 2nd Row -->
						<div class="entry">
							<h2>EDUCATION</h2>
							<div class="content">
								<h3><?php echo $from_year." - ".$to_year?></h3>
								
								<b><p><em><?php echo $degree; ?></em></p></b>
								<p ><a href='https://www.google.com/search?q=Parul+University' style="color:black"/>Parul University, India</a> <br /></p>
							</div>
							
						</div>
						
						
						<div class="entry">
						  <h2>SKILLS</h2>
							<div class="content">
								<h3>Relevant Skills</h3>
								<ul class="unordered">
									<li><?php echo $skill1; ?></li>
									<li><?php echo $skill2; ?></li>
									<li><?php echo $skill3; ?></li>
								</ul>
							</div>
							
							<div class="entry">
									
									<button align="center"><?php echo "<a href='../recviewapplicants.php?id=$job_id'>";?>< Back</a></button>
									&nbsp &nbsp &nbsp &nbsp &nbsp <button align="center"><?php echo "<a href='../recdelapplicant.php?id=$job_id&std_id=$stud_id'>";?>Reject</a></button>
									&nbsp &nbsp &nbsp &nbsp &nbsp <button align="right"><?php echo "<a href='../recacceptstud.php?id=$job_id&std_id=$stud_id'>";?>Accept</a></button>
							</div>
						</div>
						<!-- End 4th Row -->
						 <!-- Begin 5th Row -->
						<div class="entry">
									
									<div id="message"><a href="#top" >Go to Top</a></div><div id="message"><a href="#top" id="top-link">Go to Top</a></div>

						</div>
						<!-- Begin 5th Row -->
					</div>
				  <div class="clear"></div>
				  <div class="paper-bottom"></div>
				</div>
				<!-- End Paper -->
			</div>
			<div class="wrapper-bottom"></div>
		</div>
		<div id="message"><a href="#top" id="top-link">Go to Top</a></div>
		<!-- End Wrapper -->
	</body>
</html>


