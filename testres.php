<!DOCTYPE HTML>
<?php
	require('dbcon.php');
	$sql = "SELECT * FROM degrees";
														$result = mysqli_query($con,$sql);
														$count = mysqli_num_rows($result);
														$i=1;
?>
<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	</head>
	<body>
		<body class="is-preload">
		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
		<!-- Wrapper -->
			<div id="wrapper">
				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
						<h1 align="center">Student Registration</h1>
 
						<form method="POST" action="stud_registration.php">  
								<table> 	
									<tr><td>Name:</td><td><input type="text" name="name" required id="name" required></td></tr>
									<tr><td>Date of birth:</td><td><input STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="date" name="dob" required id="dob" required></td></tr>
									<tr><td>Email:</td><td><input type="email" name="email" required id="email"></td></tr>
									<tr><td>Phone:</td><td><input STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="number" name="phone" required ></td></tr>
								
								<table  id="dynamic_field">
								<tr>
									<td>Level:</td><td><select name="level">
																				<option>Diploma</option>
																				<option>Bachelors Degree</option>
																				<option>Masters Degree</option>
																				<option>Doctoral Degree</option>
																		</select></td></tr>
									<tr><td>Degree:</td><td><select name="degree" class="form-control">
																				<?php 
																					//display list of hostels in the database
																					$sql = "SELECT * FROM degrees";
																					$result = mysqli_query($con,$sql);
																					$count = mysqli_num_rows($result);
																					echo $count,"<br>";
																					if($count<=0)
																					{
																						echo "<option>No degrees available</option>";
																					}
																					else
																					{
																					while ($row = mysqli_fetch_array($result))
																					{
																						echo "<option value='" . $row['id'] ."'>".$row['name']."</option>";
																					}
																					}
																				?>
																		</select></td></tr>
									<tr><td>Graduation year:</td><td><input class="form-control" STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="number" name="grad_year" required pattern=".{3,}" maxlength="4" min="2000" max="3000"></td></tr>
									
								</tr>
								</table>
								<tr><td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td></tr>
						</table>
						<input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />
					</div>
				</form>
			</div>
		</div>
	</body>
</html>

<script>
$(document).ready(function(){
	var i=1;
	$('#add').click(function(){
		i++;
		$('#dynamic_field').append('<div id="row"><tr><td>Level:</td><td><select name="level"><option>Diploma</option><option>Bachelors Degree</option><option>Masters Degree</option><option>Doctoral Degree</option></select></td></tr><tr><td>Degree:</td><td><select name="degree" class="form-control"><?php //$sql = "SELECT * FROM degrees";$result = mysqli_query($con,$sql);$count = mysqli_num_rows($result);echo $count,"<br>";if($count<=0){echo "<option>No degrees available</option>";}else{while ($row = mysqli_fetch_array($result)){echo "<option value='" . $row['id'] ."'>".$row['name']."</option>";}}?></select></td></tr><tr><td>Graduation year:</td><td><input class="form-control" STYLE="color: rgba(255, 255, 255, 0.55); background-color: #312450;" type="number" name="grad_year" required pattern=".{3,}" maxlength="4" min="2000" max="3000"></td></tr><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></div>');});
	
	$(document).on('click', '.btn_remove', function(){
		var button_id = $(this).attr("id"); 
		$('#row').remove();
	});
	
	$('#submit').click(function(){		
		$.ajax({
			url:"name.php",
			method:"POST",
			data:$('#add_name').serialize(),
			success:function(data)
			{
				alert(data);
				$('#add_name')[0].reset();
			}
		});
	});
	
});
</script>
