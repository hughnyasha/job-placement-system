<!DOCTYPE HTML>
<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])) {
		$id = $_SESSION['id'];
	} else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: stud_login.php');
	}
?>

<html>
	<head>
		<title>Home</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
	</head>
	<body class="is-preload">

		<!-- Header -->
			
			
			
			<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome <?php echo $_SESSION['name'];?> </h3>
					<nav>
						<ul>
							<li><a align="right" href="studviewjobs.php" >Job Posts</a></li>
							<li><a align="right" href="myapps.php" >My Applications</a></li>
							<li><a align="right" href="upload_doc.php" >Upload Documents</a></li>
							<li><a align="right" href="viewdocs.php" >Documents</a></li>
							<li><a align="right" href="createresume.php" >Create Resume</a></li>
							<li><a align="right" href="purple/viewresume.php" >My Resume</a></li>
							<li><a align="right" href="stud_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
			<div id="wrapper">
				<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
				
			</div>
		<!-- Wrapper -->
			<div id="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">

				<!-- Main -->
					<section id="main" class="wrapper" >
						<div class="inner" >
						
						<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
						
						</div>
					</section>
			
			</div>

		<!-- Footer -->
			

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>