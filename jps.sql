-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2020 at 04:35 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jps`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL,
  `stud_id` int(10) NOT NULL,
  `date` varchar(13) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `post_id`, `stud_id`, `date`, `status`) VALUES
(0, 6, 1, '17-08-2020', ''),
(0, 2, 1, '17-08-2020', ''),
(0, 3, 1, '17-08-2020', ''),
(0, 4, 1, '17-08-2020', '');

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE `degrees` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `name`) VALUES
(1, 'BCA'),
(2, 'IMCA'),
(3, 'MCA'),
(4, 'Aeronautical Engineering'),
(5, 'Social Work'),
(6, 'Ayurved');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `stud_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `document` varchar(200) NOT NULL,
  `date_uploaded` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`stud_id`, `name`, `document`, `date_uploaded`) VALUES
(2, 'Student_Grade_Mark_History_MUDOTI_NYASHA_H.pdf', 'Student Marksheet', '07-08-2020'),
(2, 'NOC certificate - Nyasha .pdf', 'Other', '07-08-2020'),
(2, 'Attedance Certificate - Nyasha .pdf', 'Other', '07-08-2020'),
(2, 'Last stamp.pdf', 'Other', '07-08-2020'),
(2, 'pic.jpg', 'Other', '16-08-2020'),
(2, 'bonafide.pdf', 'Other', '18-08-2020'),
(2, 'pic.jpg', 'Graduation Certificate', '18-08-2020'),
(2, 'Attedance Certificate - Nyasha .pdf', 'Graduation Certificate', '18-08-2020'),
(1, 'Certificate.pdf', 'Identification Documents', '18-08-2020');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) NOT NULL,
  `rec_id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `company` varchar(50) NOT NULL,
  `Place` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `job_type` varchar(10) NOT NULL,
  `work_type` varchar(13) NOT NULL,
  `salary` varchar(10) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `deadline` varchar(15) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `rec_id`, `title`, `company`, `Place`, `industry`, `job_type`, `work_type`, `salary`, `description`, `deadline`, `status`) VALUES
(1, 0, 'PHP Developer', 'HUGHTECH', 'Baroda', 'Computers', 'Job', 'Full Time', '38', '<p>Please make sure to include all the details of the job responsibilities, requirents and more. Make use of bullets to make your description more presentable. Your job posting may not be approved if it is ambigous or not presentable.</p><ul><li>grdx</li></ul>', '2020-08-27', 'pending'),
(2, 0, 'Pilot', 'ASUS', 'Bangalore', '', 'Internship', 'Full Time', '20000', '<ul><li>Presentable</li><li>CLean</li><li>Intelligent</li></ul>', '2020-08-30', 'pending'),
(3, 0, 'Newcaster', 'ASUS', 'Vadodara', '', 'Job', 'Part Time', '30000', '<ol><li>English</li><li>Creative</li><li>Communication skills</li></ol>', '2020-08-30', 'pending'),
(4, 0, 'Mixer', 'ASUS', 'Bangalore', '', 'Job', 'Part Time', '51000', '<p>Please make sure to include all the details of the job responsibilities, requirents and more. Make use of bullets to make your description more presentable. Your job posting may not be approved if it is ambigous or not presentable.</p>', '2020-08-29', 'pending'),
(5, 0, 'IT', 'ASUS', 'Baroda', '', 'Job', 'Full Time', '54541', '<p>Please make sure to include all the details of the job responsibilities, requirents and more. Make use of bullets to make your description more presentable. Your job posting may not be approved if it is ambigous or not presentable.</p>', '2020-09-05', 'pending'),
(6, 5, 'adw', 'ASUS', 'guyky', '', 'Job', 'Full Time', '27887', '<p>Please make sure to include all the details of the job responsibilities, requirents and more. Make use of bullets to make your description more presentable. Your job posting may not be approved if it is ambigous or not presentable.</p>', '2020-09-06', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `recruiters`
--

CREATE TABLE `recruiters` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `company` varchar(50) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `sector` varchar(20) NOT NULL,
  `industry` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recruiters`
--

INSERT INTO `recruiters` (`id`, `name`, `company`, `email`, `phone`, `password`, `sector`, `industry`) VALUES
(4, 'hugh mudoti', 'HUGHTECH', 'hugh@gmail.com', '9016673235', '013700d5', 'Public', 'industry'),
(5, 'Nyasha', 'ASUS', 'asus@asus.com', '90166732289', '00620062', 'Private', 'industry');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` varchar(10) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone` bigint(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(20) NOT NULL,
  `degree` varchar(20) NOT NULL,
  `graduation_year` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `dob`, `email`, `phone`, `password`, `level`, `degree`, `graduation_year`) VALUES
(1, 'hugh mudoti', '2020-08-03', 'hughnyasha@gmail.com', 9016673235, '00620062', 'Diploma', '1', '2021'),
(2, 'test', '2020-07-28', 'test@test.com', 9016673225, '00620062', 'Bachelors Degree', '1', '2027');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recruiters`
--
ALTER TABLE `recruiters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `recruiters`
--
ALTER TABLE `recruiters`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
