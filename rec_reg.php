<!DOCTYPE HTML>
<?php
	require('dbcon.php');
?>
<html>
	<head>
		<title>Recruiter Registration</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<!-- Header -->
			
		<!-- Wrapper -->
				<section id="sidebar">
						<div class="inner">
							<h3 align="right">Welcome </h3>
							<nav>
								<ul>
									<li><a align="right" href="index.html" >Home</a></li>
									<li><a align="right" href="rec_login.php" >Sign In</a></li>
								</ul>
							</nav>
						</div>
					</section>
		
			<div id="wrapper" style="background-image: url('images/740570.jpg'); background-size: cover; background-repeat: no-repeat">
				<!-- Main -->
					<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
					<section id="main" class="wrapper">
						
						<div class="inner">
						<h1 align="center">Recruiter Registration</h1>
 
	<form method="POST" action="rec_registration.php">  
	<table> 	
        <tr><td>Name:</td><td><input type="text" name="name" required id="name" required></td></tr>
		  <tr><td>Company Name:</td><td><input type="text" name="company" required id="name" required></td></tr>
        <tr><td>Email:</td><td><input type="email" name="email" required id="email"></td></tr>
			<tr><td>Phone:</td><td><input style="background-color:purple" type="number" name="phone" required ></td></tr>
			<tr><td>Sector:</td><td><select name="sector">
													<option>Public</option>
													<option>Private</option>
											</select></td></tr>
			<tr><td>Industry:</td><td><select name="industry" >
													<option>Aerospace Industry</option>
													<option>Transport Industry</option>
													<option>Engineering</option>
													<option>Computers</option>
													<option>Telecommunication</option>
													<option>Agriculture</option>
													<option>Construction</option>
													<option>Education</option>
													<option>Pharmaceutical</option>
													<option>Food</option>
													<option>Health Care</option>
													<option>Hospitality</option>
													<option>Entertainment</option>
													<option>News Media</option>
													<option>Energy</option>
													<option>Manufacturing</option>
													<option>Music</option>
													<option>Electronics</option>
											</select></td></tr>
        <tr><td>Password:</td><td><input type="password" name="password" id="pass" onkeyup="validate()" required ></td></tr>
        <tr><td>Confirm Password:</td><td><input type="password" name="cpassword" id="cpass" required></td></tr>
		</table>
		
		<table>
        <tr ><td colspan="2" align="center"><input type="submit" name="submit"></td></tr>
		</table>
		</form>
    
						</div>
					</section>

			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>