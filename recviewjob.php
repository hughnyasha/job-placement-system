<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])&& isset($_SESSION['company'])) 
	{	
	$id = $_SESSION['id'];
	$name= $_SESSION['name'];
	$company = $_SESSION['company'];
	} 
	else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: rec_login.php');
		}

	
	
	
?>

<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome </h3><h2><?php echo $_SESSION['name'];?> </h2>
					<nav>
						<ul>
							<li><a align="right" href="rec_home.php" >Home</a></li>
							<li><a align="right" href="postjob.php" >Post New Job</a></li>
							<li><a align="right" href="rec_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		
		<!-- Wrapper -->
			<div id="wrapper">
					<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>

				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/740570.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
							<h2 align="center">Job Postings</h2>
							<table>
								<th>Job Title</th><th>No. of Applicants</th><th>Deadline</th>
								</html>
								<?php
										$query="Select * from jobs where company='$company'";
										$result= mysqli_query($con,$query);
										if(!$result)
										{
											die("error");
										}
										
										
										$count=mysqli_num_rows($result);
										if($count<=0)
												{
													echo "No jobs posted!";
												}
										else
												{
													
													while ($row = mysqli_fetch_array($result))
															{
																$id = $row['id'];
																$numquery="Select * from applicants where post_id='$id'";
																$numresult= mysqli_query($con,$numquery);
																$num = mysqli_num_rows($numresult);
																echo "<tr><td><a href='recappview.php?title=$id'>".$row['title']."</a><td>".$num."</td><td>".$row['deadline']."</td></tr>";
															}
												}
								?>
								<html>
							</table>
							<br><br><br><br><br>
						</div>
					</section>

			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>