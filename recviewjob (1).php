<?php
	require('dbcon.php');
	session_start();
	if (isset($_SESSION['name'])&& isset($_SESSION['company'])) 
	{	
	$id = $_SESSION['id'];
	$name= $_SESSION['name'];
	$company = $_SESSION['company'];
	} 
	else {
		echo '<script>alert("Login in first")</script>';
		//header("refresh:0;url=login");
		header('Location: rec_login.php');
		}

	
	
	
?>

<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<section id="main" class="wrapper">
						<div class="inner">
							<table>
								<th>Job Title</th><th>No. of Applicants</th><th>Deadline</th>
								</html>
								<?php
										$query="Select * from jobs where company='$company'";
										$result= mysqli_query($con,$query);
										if(!$result)
										{
											die("error");
										}
										
										
										$count=mysqli_num_rows($result);
										if($count<=0)
												{
													echo "No documents available";
												}
										else
												{
													while ($row = mysqli_fetch_array($result))
															{
																echo "<tr><td><a href='#'>".$row['title']."</a><td>0</td><td>".$row['deadline']."</td></tr>";
															}
												}
								?>
								<html>
							</table>
						</div>
					</section>

			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>