<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<section id="main" class="wrapper">
						<div class="inner">
						<h1 align="center">Password Reset</h1>
							<form method="post" action="recpassreset.php">
<table align="center">
<tr>
	<th align="left">Email         </th>
	<td><input type="email" name="email"></td>
</tr>
<tr>
	<td colspan="2" align="center">
	<input type="submit" value="     Reset    ">
	</td>
</tr>



</table>
</form>
						</div>
					</section>
<a   href="stud_reg.php">< Back to Login</a>
			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>