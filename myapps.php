<?php
	require('dbcon.php');

session_start();

if (isset($_SESSION['name'])) {
    $id = $_SESSION['id'];
} else {
    echo '<script>alert("Login in first")</script>';
    //header("refresh:0;url=login");
    header('Location: stud_login.php');
}
?>


<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
			<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome </h3><h2><?php echo $_SESSION['name'];?> </h2>
					<nav>
						<ul>
							<li><a align="right" href="stud_home.php" >Home</a></li>
							<li><a align="right" href="studviewjobs.php" >Job Posts</a></li>
							<li><a align="right" href="upload_doc.php" >Upload Documents</a></li>
							<li><a align="right" href="viewdocs.php" >Documents</a></li>
							<li><a align="right" href="createresume.php" >Create Resume</a></li>
							<li><a align="right" href="purple/viewresume.php" >My Resume</a></li>
							<li><a align="right" href="stud_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		<!-- Header -->
			

		<!-- Wrapper -->
			<div id="wrapper">
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
				
				<!-- Main -->
					<section id="main" class="wrapper" style="background-image: url('images/1655340.jpg'); background-size: cover; background-repeat: no-repeat">
						<div class="inner">
							<h2 align="center">Jobs Applied</h2>
							<table border="1">
								<th>Job Title</th><th>No. of Applicants</th><th>Status</th>
								<?php

											$query="Select * from applicants where stud_id='$id'";
											$result= mysqli_query($con,$query);
											if(!$result)
											{
												die("error");
											}
											
											else{
												
											$count=mysqli_num_rows($result);
											if($count<=0)
													{
														echo "<tr><td></td><td align='center'>No applications!</td><td></td></tr>";
													}
											else
													{
														while ($row = mysqli_fetch_array($result))
																{
																	$post_id = $row['post_id'];
																	$status = $row['status'];
																	$date = $row['date'];
																	$numquery = "Select * from applicants where post_id='$post_id'";
																	$numresult= mysqli_query($con,$numquery);
																	$numcount=mysqli_num_rows($numresult);
																	
																	$sqlquery="Select * from jobs where id='$post_id'";
																	$result1= mysqli_query($con,$sqlquery);
																	$row2 = mysqli_fetch_array($result1);
																	
																	
																	echo "<tr><td><a href='appview.php?title=$id'>".$row2['title']."</a><td>".$numcount."</td><td>".$status."</td></tr>";
																}
													}
										}
										
								?>
							
							</table>
						</div>
					<br><br><br><br><br><br><br><br><br>
					</section>
					
			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>