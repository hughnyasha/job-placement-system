<?php
	require('dbcon.php');

session_start();

if (isset($_SESSION['name'])) {
    $id = $_SESSION['id'];
	$job_id=$_GET['id'];
} else {
    echo '<script>alert("Login in first")</script>';
    //header("refresh:0;url=login");
    header('Location: stud_login.php');
}
?>


<html>
	<head>
		<title>Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
			<section id="sidebar">
				<div class="inner">
					<h3 align="right">Welcome </h3><h2><?php echo $_SESSION['name'];?> </h2>
					<nav>
						<ul>
							<li><a align="right" href="rec_home.php" >Home</a></li>
							<li><a align="right" href="recviewjob.php" >Job Posts</a></li>
							<li><a align="right" href="postjob.php" >Post new job</a></li>
							<li><a align="right" href="rec_logout.php">Logout</a></li>
						</ul>
					</nav>
				</div>
			</section>
		<!-- Header -->
			

		<!-- Wrapper -->
			<div id="wrapper" style="background-image: url('images/740570.jpg'); background-size: cover; background-repeat: no-repeat">
			<header id="header">
				<a href="index.html" class="title">Job Placement System</a>
			</header>
				
				<!-- Main -->
					<section id="main" class="wrapper">
						<div class="inner">
							<h2 align="center">Applicants</h2>
							<table border="1">
								<th>Name</th><th>Date Applied</th><th>Status</th>
								<?php
										
											$query="Select * from applicants where post_id=$job_id";
											$result= mysqli_query($con,$query);
											if(!$result)
											{
												die("error");
											}
											
											else
											{
												$count=mysqli_num_rows($result);
												if($count<=0)
														{
															echo "<tr><td colspan='3' align='center'>No applicants yet</td></tr>";
														}
												else
														{
															while ($row = mysqli_fetch_array($result))
																	{
																		$stud_id = $row['stud_id'];
																		$date = $row['date'];
																		$status = $row['status'];
																		$stud_query="Select * from students where id=$stud_id";
																		$stud_result= mysqli_query($con,$stud_query);
																		
																			$stud_row = mysqli_fetch_array($stud_result);
																			
																			if(!$stud_row)
																			{
																				echo "error";
																			}	
																			else
																			{
																			$stud_name = $stud_row['name'];
																			echo "<tr><td><a href='purple/recviewresume.php?id=$stud_id&job_id=$job_id'>".$stud_name."</a></td><td>".$date."</td><td>".$status."</td></tr>";
																			}
																	}
														}
											}
								?>
							
							</table>
						</div>
					<br><br><br><br><br><br><br><br><br>
					</section>
					
			</div>

		<!-- Footer -->
			<footer id="footer" class="wrapper alt">
				<div class="inner">
					<ul class="menu">
						<li>&copy; Job Placement System. All rights reserved.</li><li>Developed by: Leeton Dida and Nyasha Mudoti</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>